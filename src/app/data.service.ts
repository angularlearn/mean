import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class DataService {

  public result: any;

  constructor(private _http: Http) { }

  getPosts() {
    return this._http.get("/api/posts")
      .pipe(map(result => this.result = result.json().data));
  }

}